﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using YASA.Engine.DAO;
using YASA.Engine.DTO;
using YASA.Engine.Numeric;

namespace YASA.Engine
{
    class IntegratedActiveContoursTextureSegmentationAlgorithm: ITextureSegmentationAlgorithm
    {
        private GaborEmbeddingMap _embeddingMap;

        private readonly int _mapHeight;
        private readonly int _mapWidth;

        private readonly VectorAnalysis _analysis = VectorAnalysis.GetInstance();
        private readonly GaborMetricsCalculator _metricsCalculator = GaborMetricsCalculator.GetInstance();

        private const double BeltramiFlowSimulationSpan = 20.0;
        private const double BeltramiFlowSimulationStep = 0.1;

        private const double LevelSetSimulationSpan = 10;
        private const double LevelSetSimulationStep = 0.1;

        private double[,] _levelSetFunctionValues;

        public IntegratedActiveContoursTextureSegmentationAlgorithm(ref GaborFeatureSpaceElement[,] featureSpace)
        {
            _embeddingMap = GaborEmbeddingMapDao.GetGaborEmbeddingMap(ref featureSpace);

            _mapHeight = _embeddingMap.FrequencyFunction.GetLength(0);
            _mapWidth = _embeddingMap.FrequencyFunction.GetLength(1);

            SmoothOutFetures();
        }

        private void SmoothOutFetures()
        {
            SmoothOuFeature(ref _embeddingMap.RealResponseFunction);
            SmoothOuFeature(ref _embeddingMap.ImgResponseFunction);
            SmoothOuFeature(ref _embeddingMap.FrequencyFunction);
            SmoothOuFeature(ref _embeddingMap.OrientationFunction);
            SmoothOuFeature(ref _embeddingMap.ScaleFunction);
        }

        private void SmoothOuFeature(ref double[,] feature)
        {
            double[, ,] data = new double[_mapHeight, _mapWidth, 1];

            for (int i = 0; i < _mapHeight; i++)
            {
                for (int j = 0; j < _mapWidth; j++)
                {
                    data[i, j, 0] = feature[i, j];
                }
            }

            Image<Gray, double> img = new Image<Gray, double>(data);
            // img.Save("edge_no_smoothing.bmp");
            img = img.SmoothGaussian(15);

            for (int y = 0; y < _mapHeight; y++)
            {
                for (int x = 0; x < _mapWidth; x++)
                {
                    feature[y, x] = img.Data[y, x, 0];
                }
            }
        }

        private void CoupledBeltramiFlowDiffusion()
        {
            double[,] determinantValues = new double[_mapHeight,_mapWidth];
            double[,] squareRootOfDeterminantValues = new double[_mapHeight, _mapWidth];

            Vector2D[,] realVectorField = Util.Util.AllocateVectorField(_mapWidth, _mapHeight);
            Vector2D[,] imgVectorField = Util.Util.AllocateVectorField(_mapWidth, _mapHeight);
            Vector2D[,] frequencyVectorField = Util.Util.AllocateVectorField(_mapWidth, _mapHeight);
            Vector2D[,] scaleVectorField = Util.Util.AllocateVectorField(_mapWidth, _mapHeight);
            Vector2D[,] orientationVectorField = Util.Util.AllocateVectorField(_mapWidth, _mapHeight);

            for (double t = 0; t < BeltramiFlowSimulationSpan; t+=BeltramiFlowSimulationStep)
            {
                for (int y = 0; y < _mapHeight; y++)
                {
                    for (int x = 0; x < _mapWidth; x++)
                    {
                        determinantValues[y, x] =
                            1/_metricsCalculator.GetGaborFeatureMetricsDeterminantAtPoint(ref _embeddingMap, x, y);
                        squareRootOfDeterminantValues[y, x] = Math.Sqrt(1 + determinantValues[y, x]);
                    }
                }

                
               for (int y = 0; y < _mapHeight; y++)
                {
                    for (int x = 0; x < _mapWidth; x++)
                    {
                        Vector2D real = _analysis.CalculateGradientOnDomain(ref determinantValues,
                            ref _embeddingMap.RealResponseFunction, x, y);
                        Vector2D img = _analysis.CalculateGradientOnDomain(ref determinantValues,
                            ref _embeddingMap.ImgResponseFunction, x, y);
                        Vector2D scale = _analysis.CalculateGradientOnDomain(ref determinantValues,
                            ref _embeddingMap.ScaleFunction, x, y);
                        Vector2D frequency = _analysis.CalculateGradientOnDomain(ref determinantValues,
                            ref _embeddingMap.FrequencyFunction, x, y);
                        Vector2D orientation = _analysis.CalculateGradientOnDomain(ref determinantValues,
                            ref _embeddingMap.OrientationFunction, x, y);

                        double denominator = 1/(2*squareRootOfDeterminantValues[y, x]);
                       
                        realVectorField[y, x].X = real.X / denominator;
                        realVectorField[y, x].Y = real.Y / denominator;

                        imgVectorField[y, x].X = img.X/ denominator;
                        imgVectorField[y, x].Y = img.Y / denominator;

                        orientationVectorField[y, x].X = orientation.X / denominator;
                        orientationVectorField[y, x].Y = orientation.Y / denominator;

                        frequencyVectorField[y, x].X = frequency.X / denominator;
                        frequencyVectorField[y, x].Y = frequency.Y / denominator;

                        scaleVectorField[y, x].X = scale.X / denominator;
                        scaleVectorField[y, x].Y = scale.Y / denominator;
                    }
                }
                CoupledBeltramiFlowUpdate(ref realVectorField, ref imgVectorField, ref orientationVectorField, ref scaleVectorField, ref frequencyVectorField, ref squareRootOfDeterminantValues);
            }
            SmoothOutFetures();
            Util.Util.SaveMatrix(ref _embeddingMap.RealResponseFunction, "realResponse");
            Util.Util.SaveMatrix(ref _embeddingMap.ImgResponseFunction, "imgResponse");
            Util.Util.SaveMatrix(ref _embeddingMap.ScaleFunction, "scaleResponse"); 
            Util.Util.SaveMatrix(ref _embeddingMap.OrientationFunction, "orientationResponse");
            Util.Util.SaveMatrix(ref _embeddingMap.FrequencyFunction, "frequencyResponse");
            
        }
        //TODO AAAAAAA! SIMPLIFY!!!!
        

        private void CoupledBeltramiFlowUpdate(ref Vector2D[,] realField, ref Vector2D[,] imgField, ref Vector2D[,] orientationField, ref Vector2D[,] scaleField, ref Vector2D[,] frequencyField, ref double[,] squareRootsOfDet)
        {
            for (int i = 0; i < _mapHeight; i++)
            {
                for (int j = 0; j < _mapWidth; j++)
                {
                    _embeddingMap.RealResponseFunction[i, j] += 1/squareRootsOfDet[i, j] * _analysis.CalculateDivergence(ref realField, j, i);

                    _embeddingMap.ImgResponseFunction[i, j] += 1 / squareRootsOfDet[i, j] *
                                                                _analysis.CalculateDivergence(ref imgField, j, i);

                    _embeddingMap.FrequencyFunction[i, j] += 1 / squareRootsOfDet[i, j] *
                                                                _analysis.CalculateDivergence(ref frequencyField, j, i);

                    _embeddingMap.OrientationFunction[i, j] += 1 / squareRootsOfDet[i, j] *
                                                                _analysis.CalculateDivergence(ref orientationField, j, i);

                    _embeddingMap.ScaleFunction[i, j] += 1 / squareRootsOfDet[i, j] *
                                                                _analysis.CalculateDivergence(ref scaleField, j, i);
                }
            }
        }

        public double[,] GetStoppingTermFunction()
        {
            double[,] stoppingTermFunction = new double[_mapHeight, _mapWidth];

            for (int y = 0; y < _mapHeight; y++)
            {
                for (int x = 0; x < _mapWidth; x++)
                {
                    stoppingTermFunction[y, x] = 1/
                                                 (1 +
                                                  _metricsCalculator.GetGaborFeatureMetricsDeterminantAtPoint(
                                                      ref _embeddingMap, x, y));
                }
            }

            
            SmoothOuFeature(ref stoppingTermFunction);
            Util.Util.SaveMatrix(ref stoppingTermFunction, "stoppingTerm");

            return stoppingTermFunction;
        }

        private void LevelSetFlowUpdate(ref Vector2D[,] field, ref double[,] gradientAbs)
        {
            for (int i = 0; i < _mapHeight; i++)
            {
                for (int j = 0; j < _mapWidth; j++)
                {
                    _levelSetFunctionValues[i, j] += gradientAbs[i,j]*_analysis.CalculateDivergence(ref field, j, i);
                    if (Math.Abs(_levelSetFunctionValues[i, j]) > 2)
                    {
                        _levelSetFunctionValues[i, j] = 1;
                    }

                }
            }            
        }

        

        public LinkedList<Point> GetBorderPoints()
        {
            CoupledBeltramiFlowDiffusion();

            double[,] stoppingTermFunction = GetStoppingTermFunction();
            _levelSetFunctionValues = GetStoppingTermFunction();
            LinkedList<Point> points = new LinkedList<Point>();
            
            Vector2D[,] field = Util.Util.AllocateVectorField(_mapWidth, _mapHeight);

            double[,] gradientMagnitudes = new double[_mapHeight,_mapWidth];
            for (double t = 0; t < LevelSetSimulationSpan; t += LevelSetSimulationStep)
            {
                for (int y = 0; y < _mapHeight; y++)
                {
                    for (int x = 0; x < _mapWidth; x++)
                    {
                        Vector2D levelSetGradient = _analysis.CalculateGradient(ref _levelSetFunctionValues, x, y);
                        double levelSetGradientAbs = _analysis.GetEuclideanMetricLength(ref levelSetGradient);

                        field[y, x].X = stoppingTermFunction[y, x]*levelSetGradient.X/levelSetGradientAbs;
                        field[y, x].Y = stoppingTermFunction[y, x]*levelSetGradient.Y/levelSetGradientAbs;

                        gradientMagnitudes[y, x] = levelSetGradientAbs;
                    }
                }


                LevelSetFlowUpdate(ref field, ref gradientMagnitudes);
            }
            //SmoothOuFeature(ref _levelSetFunctionValues);

            double min = Util.Util.MinValue(ref _levelSetFunctionValues);
            for (int i = 0; i < _mapHeight; i++)
            {
                for (int j = 0; j < _mapWidth; j++)
                {
                    if (_levelSetFunctionValues[i, j] <= 1.80*min)
                    {
                        points.AddLast(new Point(j, i));
                    }
                }
            }

            
            //
            Util.Util.SaveMatrix(ref _levelSetFunctionValues, "levelSet");

            return points;
        }

        
    }
}
