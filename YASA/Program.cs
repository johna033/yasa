﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using YASA.Engine;
using YASA.Engine.DTO;

namespace YASA
{
    class Program
    {
        static void Main(string[] args)
        {
            Image<Gray, float> image = new Image<Gray, float>(@"~/../../../dataset/zebra1.jpg");
            ImageViewer.Show(image);

            //double[] orientations = {0, Math.PI/8, 2*Math.PI/8, 3*Math.PI/8, 4*Math.PI/8, 5*Math.PI/8, 6*Math.PI/8, 7*Math.PI/8};
            //double[] scales = {0.8638,0.9070,0.9525,1};
            //double[] frequencies = {0.15};
           double[] orientations = { 0, Math.PI / 6, Math.PI / 4, Math.PI / 3, 2*Math.PI / 3, 3 * Math.PI / 4, 5 * Math.PI / 6};
           double[] scales = { 1,2,3};
          double[] frequencies = {0.225,0.3,0.375};

            GaborFeatureSpaceElement[,] gaborFeatureSpace =
                GaborFeatureSpaceGenerator.GetInstance().GenerateFutureSpaceWithMaximalResponseCoefficients(image, orientations,
                    scales, frequencies);
            Console.WriteLine("Gabor feature space generated");

            IntegratedActiveContoursTextureSegmentationAlgorithm segAlgorithm = new IntegratedActiveContoursTextureSegmentationAlgorithm(ref gaborFeatureSpace);

            Stopwatch sw = new Stopwatch();
            sw.Start();
             LinkedList<Point> border = segAlgorithm.GetBorderPoints();
            sw.Stop();
            Image<Rgb, float> image1 = new Image<Rgb, float>(@"~/../../../dataset/zebra1.jpg");
            foreach (Point point in border)
            {
                image1[point] = new Rgb(Color.Yellow);
            }

            ImageViewer.Show(image1);
            image1.Save(@"~/../../../dataset/segmented.jpg");
            Console.WriteLine("Done segmenting. Time: "+sw.Elapsed);

            Console.Read();
        }
    }
}
